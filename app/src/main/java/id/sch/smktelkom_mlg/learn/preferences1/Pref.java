package id.sch.smktelkom_mlg.learn.preferences1;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hyuam on 16/02/2017.
 */

public class Pref
{

    public static final String MY_PREFS = "MyPrefs";
    public static final String EMAIL = "Email";
    public static final String PASSWORD = "Password";

    protected static void setPrefString(Context context, String key, String value)
    {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    protected static String getPrefString(Context context, String key)
    {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }

    public static void setEmail(Context context, String value)
    {
        setPrefString(context, EMAIL, value);
    }

    public static void setPass(Context context, String value)
    {
        setPrefString(context, PASSWORD, value);
    }

    public static String getEmail(Context context)
    {
        return getPrefString(context, EMAIL);
    }

    public static String getPass(Context context)
    {
        return getPrefString(context, PASSWORD);
    }
}
