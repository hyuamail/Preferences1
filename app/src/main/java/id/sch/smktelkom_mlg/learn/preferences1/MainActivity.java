package id.sch.smktelkom_mlg.learn.preferences1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String email = Pref.getEmail(this);

        TextView tv = (TextView) findViewById(R.id.textView);
        tv.setText("Selamat Datang User " + email);
    }
}
